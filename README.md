### Hi, I'm Bram 👋

You can reach me on the contact details listed on https://bramh.me, or via:

**mail**: bram@bramh.me  
**discord**: bramh  
**pgp**: [499DE67900B12A11](https://bramh.me/public.asc)  
